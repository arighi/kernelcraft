from pathlib import Path

VERSION = '2.0'
CONF_PATH = Path(Path.home(), '.config', 'virtme-ng')
CONF_FILE = Path(CONF_PATH, 'virtme-ng.conf')
